/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS103: Rewrite code to no longer use __guard__, or convert again using --optional-chaining
 * DS206: Consider reworking classes to avoid initClass
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/main/docs/suggestions.md
 */

let Tags;
module.exports = (Tags = (function() {
	let tagsDb = undefined;
	let tags = undefined;
	Tags = class Tags {
		static initClass() {
			this.prototype.view = __dirname;
			this.prototype.name = 'k-tags-flat';
			tagsDb = undefined;
			tags = undefined;
		}

		create() {
			this.model.fn('sort', (a, b) => a.localeCompare(b));
			tags = this.model.at('tags');
			tags.setNull([]);
			tagsDb = this.model.root.at('misc.tags');
			return tagsDb.subscribe(err => {
				tagsDb.setNull('tags', []);
				return this.model.ref('tagsDb', tagsDb.at('tags'));
			});
		}

		t(s) {
			const t = this.model.get('translate');
			if (typeof t === 'string') { return this.app.proto[t].call(this.app, s); } else { return s; }
		}

		remove(i) {
			tags.remove(i);
			return this.input.focus();
		}


		keydown(e) {
			const key = e.keyCode || e.which;
			if (key === 13) { return this.add(e); }
		}

		add(e) {
			e.preventDefault();
			if (this.input.value) {
				if (__guard__(tagsDb.get('tags'), x => x.indexOf(this.input.value)) === -1) { tagsDb.at('tags').push(this.input.value); }
				if (tags.get().indexOf(this.input.value) === -1) { tags.push(this.input.value); }
				this.input.value = '';
				return this.input.focus();
			}
		}
	};
	Tags.initClass();
	return Tags;
})());

function __guard__(value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}